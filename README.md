# c4logrus

[![Test Coverage](https://img.shields.io/badge/coverage-82%25-yellow.svg)](file:coverage.html)
[![Go Report Card](https://goreportcard.com/badge/codeberg.org/fractalqb/c4logrus)](https://goreportcard.com/report/codeberg.org/fractalqb/c4logrus)
[![GoDoc](https://godoc.org/codeberg.org/fractalqb/c4logrus?status.svg)](https://godoc.org/codeberg.org/fractalqb/c4logrus)

`import "git.fractalqb.de/fractalqb/c4logrus"`

---

# Configuration for logrus logging
